package com.bucorel.jhtml;

import java.lang.StringBuilder;
import java.util.Calendar;

public class DateElement{
	
	String name;
	int format;
	
	String d;
	String m;
	String y;
	
	public static final int DMY = 1;
	public static final int MDY = 2;
	public static final int YMD = 3;
	
	public DateElement( String id, int dateFormat ){
		name = id;
		format = dateFormat;
		Calendar c = Calendar.getInstance();
		y = Integer.toString( c.get( Calendar.YEAR ) );
		m = Integer.toString( c.get( Calendar.MONTH ) + 1 );
		d = Integer.toString( c.get( Calendar.DAY_OF_MONTH ) );
	}
	
	public void setValue(int dd, int mm, int yy ){
		d=Integer.toString( dd );
		m=Integer.toString( mm );
		y=Integer.toString( yy );
	}
	
	public String getDD( String value ){
		HtmlElement he = new HtmlElement( "select" );
		he.setAttribute( "name", name + "_d" );
		he.setAttribute( "id", name + "_d" );
		he.setSelectedValue( value );
		for( int i=1; i<32; i++ ){
			he.setOption( Integer.toString( i ), Integer.toString( i ) );
		}
		
		return he.draw();
	}
	
	public String getMM( String value ){
		HtmlElement he = new HtmlElement( "select" );
		he.setAttribute( "name", name + "_m" );
		he.setAttribute( "id", name + "_m" );
		he.setSelectedValue( value );
		for( int i = 1; i < 13; i++ ){
			he.setOption( Integer.toString( i ), Integer.toString( i ) );
		}
		
		return he.draw();
	}
	
	public String getYY( String value ){
		HtmlElement he = new HtmlElement( "input" );
		he.setAttribute( "name", name + "_y" );
		he.setAttribute( "id", name + "_y" );
		he.setAttribute( "value", value );
		
		return he.draw();
	}
	
	public String selectButton(){
		HtmlElement he = new HtmlElement("input");
		he.setAttribute("type","button");
		he.setAttribute("value","+");
		he.setAttribute("onclick","showCal('" + name + "')");
		return he.draw();
	}
	
	public String draw(){
		StringBuilder s = new StringBuilder();
		HtmlElement he = new HtmlElement( "div" );
		he.setAttribute( "class", "dateholder" );
		
		switch( format ){
			case 3:
				s.append(getYY(y));
				s.append(getMM(m));
				s.append(getDD(d));
				break;
			case 2:
				s.append(getMM(m));
				s.append(getDD(d));
				s.append(getYY(y));
				break;
			default:
				s.append(getDD(d));
				s.append(getMM(m));
				s.append(getYY(y));
				break;
		}
		s.append(selectButton());
		
		he.setInnerHtml( s.toString() );
		return he.draw();
	}
}
