package com.bucorel.jhtml;

import java.lang.StringBuilder;

public class FlexForm{
	
	String address = "";
	String submitLabel = "Submit";
	String cancelLabel = "Cancel";
	
	boolean showCancelButton = false;
	String cancelAction = "";
	
	public FlexForm( String adr ){
		address = adr;
	}
	
	public void setSubmitLabel( String label ){
		submitLabel = label;
	}
	
	public void setCancelLabel( String label ){
		cancelLabel = label;
	}
	
	public void setShowCancelButton( boolean truefalse ){
		showCancelButton = truefalse;
	}
	
	public void setCancelAction( String action ){
		cancelAction = action;
	}
	
	public String getHeader(){
		return "<div class=\"formholder\"><form method=\"POST\" action=\"\" autocomplete=\"off\">";	
	}
	
	public String getFooter(){
		return "</form></div>";
	}
	
	public String getRow( String label, String content ){
		StringBuilder html = new StringBuilder();
		html.append( "<div class=\"row\">" );
		
		/* Label */
		html.append( "<div class=\"label\">" );
		html.append( label );
		html.append( "</div>" );
		
		/* Element */
		html.append( "<div class=\"element\">" );
		html.append( content );
		html.append( "</div>" );
		
		html.append( "</div>" );
		
		return html.toString();
	}
	
	public String getHeadingRow( String headingText ){
		StringBuilder html = new StringBuilder();
		html.append( "<div class=\"row\">" );
		
		/* Label */
		html.append( "<div class=\"heading\">" );
		html.append( headingText );
		html.append( "</div>" );
		
		html.append( "</div>" );
		
		return html.toString();
	}
	
	public String getInfoRow( String info ){
		StringBuilder html = new StringBuilder();
		html.append( "<div class=\"row\">" );
		
		/* Label */
		html.append( "<div class=\"infolabel\">" );
		html.append( info );
		html.append( "</div>" );
		
		html.append( "</div>" );
		
		return html.toString();
	}

	public String getGroupRow( String title ){
		StringBuilder html = new StringBuilder();
		html.append( "<div class=\"row\">" );
		
		/* Label */
		html.append( "<div class=\"group\">" );
		html.append( title );
		html.append( "</div>" );
		
		html.append( "</div>" );
		
		return html.toString();
	}
	
	public String getButtonRow(){		
		StringBuilder html = new StringBuilder();
		html.append( "<div class=\"row\">" );
		
		/* Label */
		html.append( "<div class=\"btngroup\">" );
		
		/* Submit Button */
		HtmlElement b1 = new HtmlElement("input");
		b1.setAttribute( "type", "submit" );
		b1.setAttribute( "value", submitLabel );
		b1.setAttribute( "onclick", "pf('" + address + "', this.form)" );
		html.append( b1.draw() );
		
		/* Cancel Button */
		if( showCancelButton ){
			HtmlElement b2 = new HtmlElement("input");
			b2.setAttribute( "type", "button" );
			b2.setAttribute( "value", cancelLabel );
			b2.setAttribute( "onclick", cancelAction );
			html.append( b1.draw() );
		}
		
		html.append( "</div>" );
		
		html.append( "</div>" );
		
		return html.toString();
	}
	
	public String getHiddenField( String name, String value ){
		return "<input type=\"hidden\" name=\"" + name + "\" value=\"" + value + "\" />";
	}
}
