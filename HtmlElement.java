package com.bucorel.jhtml;

import java.lang.StringBuilder;
import java.util.LinkedHashMap;
import java.util.Map;

/**
* this class creates html elements
*/
public class HtmlElement{
	String elementType;
	String innerHtml;
	boolean hasInnerHtml;
	String selectedValue;
	LinkedHashMap<String,String> attribs;
	LinkedHashMap<String,String> options;
	
	public HtmlElement( String typeName ){
		elementType = typeName;
		innerHtml = "";
		hasInnerHtml = false;
		selectedValue = "";
		attribs = new LinkedHashMap<String,String>();
		options = new LinkedHashMap<String,String>();
		
	}
	
	public void setAttribute( String key, String value ){
		attribs.put( key, value );
	}
	
	public void setOption( String key, String value ){
		options.put( key, value );
		hasInnerHtml = true;
	}
	
	public void setInnerHtml( String value ){
		innerHtml = value;
		hasInnerHtml = true;
	}
	
	public void setSelectedValue( String value ){
		selectedValue = value;
	}
	
	public String draw(){
		StringBuilder html = new StringBuilder();
		html.append( "<" );
		html.append( elementType );
		html.append( " " );

		for (Map.Entry<String,String> entry : attribs.entrySet()) {
		  html.append( entry.getKey() );
		  html.append( "=\"" );
		  html.append( entry.getValue() );
		  html.append( "\" " );
		}

		if( !hasInnerHtml ){
			html.append( "/>" );
			return html.toString();
		}
		
		html.append( ">" );
		html.append( innerHtml );
		
		for (Map.Entry<String,String> entry : options.entrySet()) {
		  html.append( makeOption(entry.getKey(),entry.getValue()) );
		}
		
		html.append( "</" );
		html.append( elementType );
		html.append( ">" );
		return html.toString();
	}
	
	private String makeOption( String k, String v ){
		String sel = "";
		if( k.equals( selectedValue ) ){
			sel = "SELECTED";
		}
		return "<option value=\"" + k + "\" " + sel + ">" + v + "</option>\n";
	}
}
