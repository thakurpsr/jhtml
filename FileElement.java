package com.bucorel.jhtml;

public class FileElement{

	String name;
	String browseBtnLabel;
	
	public FileElement( String id ){
		name = id;
		browseBtnLabel = "Browse";
	}
	
	public void setBrowseBtnLabel( String s ){
		browseBtnLabel = s;
	}
	
	public String draw(){
		/* initialize holder DIV. This will be used to style custom file element */
		HtmlElement holder = new HtmlElement( "div" );
		holder.setAttribute( "class", "fileholder" );
		
		/* this will be used to glu all the elements */
		StringBuilder s = new StringBuilder();
		
		/* The dummy file element - it is basically a text box which will be
		used to show the name of the selected file */
		HtmlElement dummyBox = new HtmlElement("input");
		dummyBox.setAttribute("type","text");
		dummyBox.setAttribute("name", name + "_dummy" );
		dummyBox.setAttribute("id", name + "_dummy" );
		
		/* custom browse button which will trigger the file open dialog box */
		HtmlElement browseBtn = new HtmlElement("input");
		browseBtn.setAttribute("type","button");
		browseBtn.setAttribute("value", browseBtnLabel );
		browseBtn.setAttribute("onclick","openFileBrow('" + name + "')");
		
		/* this is the real file element. It will be hidden */
		HtmlElement fileBox = new HtmlElement("input");
		fileBox.setAttribute("type","file");
		fileBox.setAttribute("name", name );
		fileBox.setAttribute("id", name );
		fileBox.setAttribute("style", "display:none" );
		fileBox.setAttribute("onchange","updateFileBrow('" + name + "')");
		
		/* lets glu all the three elements together */
		s.append(dummyBox.draw());
		s.append(browseBtn.draw());
		s.append(fileBox.draw());
		
		/* add this string in to the holder */
		holder.setInnerHtml( s.toString() );
		return holder.draw();
	}
}
