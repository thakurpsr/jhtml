package com.bucorel.jhtml;

import java.util.ArrayList;
import java.lang.StringBuilder;
import java.util.LinkedHashMap;

public class FlexTable{
	
	ArrayList<FlexColumn> columnSet;
	String name;
	
	public FlexTable( String id ){
		columnSet = new ArrayList<FlexColumn>();
		name = id;
	}
	
	public void addColumn( FlexColumn column ){
		columnSet.add( column );
	}
	
	public String getHeader(){
		StringBuilder html = new StringBuilder();
		int i=0;
		
		html.append( "<div class=\"flextable\" " );
		html.append( "id=\"" );
		html.append( name );
		html.append( "\" >" );
		
		html.append( "<div class=\"row\">" );
		
		for( FlexColumn c : columnSet ){
			html.append( "<div class=\"hcell\" " );
			if( c.allowSorting ){
				html.append( "onclick=\"sortTable('");
				html.append( name );
				html.append( "'," + Integer.toString(i) + ")\"" );
			}
			html.append( " >" );
			html.append( c.title );
			html.append( "</div>" );
			i++;
		}
		
		html.append( "</div>" );
		
		return html.toString();
	}
	
	public String getRow( LinkedHashMap m ){
		StringBuilder html = new StringBuilder();
		html.append( "<div class=\"row\">" );
		
		for( FlexColumn c : columnSet ){
			html.append( "<div class=\"cell\">" );
			html.append( m.get( c.fieldName ) );
			html.append( "</div>" );
		}
		
		html.append( "</div>" );
		
		return html.toString();
	}
	
	public String getFooter(){
		return "</div>";
	}
}
