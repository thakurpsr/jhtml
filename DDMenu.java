package com.bucorel.jhtml;

import java.lang.StringBuilder;

public class DDMenu{
	
	String title;
	StringBuilder list;
	
	public DDMenu( String titleText ){
		title = titleText;
		list = new StringBuilder();
	}
	
	public void setOption( String titleTxt, String url ){
		HtmlElement he = new HtmlElement("div");
		he.setInnerHtml( titleTxt );
		he.setAttribute("class","option");
		he.setAttribute("onclick","pq('"+url+"')");
		list.append( he.draw() );
	}
	
	public String draw(){
		HtmlElement holder = new HtmlElement("div");
		holder.setAttribute("class","ddmenu");
		
		HtmlElement he = new HtmlElement("div");
		he.setInnerHtml(title);
		he.setAttribute("class","title");
		
		HtmlElement he1 = new HtmlElement("div");
		he1.setAttribute("class","list");
		he1.setInnerHtml( list.toString() );
		
		holder.setInnerHtml( he.draw() + he1.draw() );
		return holder.draw();
	}
}
