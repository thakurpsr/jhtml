package com.bucorel.jhtml;

import java.util.Calendar;

public class TimeElement{
	
	String name;
	String hrs;
	String min;
	String sec;
	
	public TimeElement( String id ){
		name = id;
		Calendar now = Calendar.getInstance();
		hrs = Integer.toString(now.get(Calendar.HOUR_OF_DAY));
		min = Integer.toString(now.get(Calendar.MINUTE));
		sec = Integer.toString(now.get(Calendar.SECOND));;
	}
	
	public void setValue( int h, int m, int s ){
		hrs = Integer.toString( h );
		min = Integer.toString( m );
		sec = Integer.toString( s );
	}
	
	public String getHrs( String value ){
		HtmlElement he = new HtmlElement( "select" );
		he.setAttribute( "name", name + "_hrs" );
		he.setAttribute( "id", name + "_hrs" );
		he.setSelectedValue( value );
		for( int i = 0; i < 24; i++ ){
			he.setOption(Integer.toString(i),Integer.toString(i));
		}
		
		return he.draw();
	}
	
	public String getMin( String value ){
		HtmlElement he = new HtmlElement( "select" );
		he.setAttribute( "name", name + "_min" );
		he.setAttribute( "id", name + "_min" );
		he.setSelectedValue( value );
		for( int i = 0; i < 60; i++ ){
			he.setOption(Integer.toString(i),Integer.toString(i));
		}
		
		return he.draw();
	}
	
	public String getSec( String value ){
		HtmlElement he = new HtmlElement( "select" );
		he.setAttribute( "name", name + "_sec" );
		he.setAttribute( "id", name + "_sec" );
		he.setSelectedValue( value );
		for( int i = 0; i < 60; i++ ){
			he.setOption(Integer.toString(i),Integer.toString(i));
		}
		
		return he.draw();
	}
	
	public String draw(){
		StringBuilder s = new StringBuilder();
		HtmlElement he = new HtmlElement("div");
		he.setAttribute("class","timeholder");
		
		s.append( getHrs( hrs ) );
		s.append( getMin( min ) );
		s.append( getSec( sec ) );
		
		he.setInnerHtml( s.toString() );
		return he.draw();
	}
}
