package com.bucorel.jhtml;

import java.io.PrintWriter;
import com.google.gson.Gson;

public class JsonResponse{
	
	Gson g = null;
	PrintWriter out = null;
	boolean started = false;
	boolean ended = false;
	boolean dataStarted = false;
	
	public JsonResponse(){
		g = new Gson();
	}
	
	public void start( int status, String msg, String field ){
		if( !ended && !started ){
			out.println("{");
			out.println( "\"_sta\":" + g.toJson(status) + "," );
			out.println( "\"_mes\":" + g.toJson(msg) + ",");
			out.println( "\"_fie\":" + g.toJson(field) + ",");
			out.println( "\"_dat\":[");
			started = true;
		}
	}
	
	public void end(){
		if( started ){
			out.println("]}");
			ended = true;
		}
	}
	
	public void send( String type, String id, String html ){
		if( !started ){
			start(1,"","");
		}
		
		if( dataStarted ){
			out.println(",");
		}else{
			dataStarted = true;
		}
		
		out.println("{");
		out.println("\"_typ\":" + g.toJson(type) + ",");
		out.println("\"_id\":" + g.toJson(id) + ",");
		out.println("\"_htm\":" + g.toJson(html) );
		out.println("}");
	}
	
	public void startLargeBlock( String type, String id ){
		if( !started ){
			start(1,"","");
		}
		
		if( dataStarted ){
			out.println(",");
		}else{
			dataStarted = true;
		}
		
		out.println("{");
		out.println("\"_typ\":" + g.toJson(type) + ",");
		out.println("\"_id\":" + g.toJson(id) + ",");
		out.print("\"_htm\":\"" );
	}
	
	public void endLargeBlock(){
		out.print("\"}");
	}
	
	public void sendBlock( String part ){
		part=g.toJson(part);
		out.print( part.replaceAll("^\"|\"$", "") );
	}
	
	public void sendOk( String msg, String field ){
		start(1,msg,field);
		end();
	}
	
	public void sendError( String msg, String field ){
		start(0,msg,field);
		end();
	}
	
	public void setFocus( String id ){
		send("js","","setFocus('"+id+"')");
	}
}
